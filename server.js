'use strict';

let express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  session = require('express-session'),
  flash = require('./middlewares/flash')

app.set('view engine', 'ejs')

app.use('/assets', express.static('public'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(session({
  secret: 'le-caca-merveilleux',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}))
app.use(flash)

app.get('/', (request, response) => {
  response.render('pages/index', { test: 'salut' })
})

app.post('/', (request, response) => {
  if (!request.body.message) {
    request.flash('error', "T'as pas remplie le form trou duc")
    response.redirect('/')
  }
  request.flash('success', "Yolo ! :D")
  response.redirect('/')
})

app.listen(8080)
